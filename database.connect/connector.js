const mysql = require("mysql");

const connect = mysql.createConnection({
  host: "localhost",
  database: "student_app",
  port: 3306,
  user: "root",
  password: "admin",
});

connect.connect((err) => {
  if (err) {
    console.log("Error connect to database!! \n AT : " + err);
  } else {
    console.log("Connect to Database Success!");
  }
});

// connect.connect;

module.exports = connect;
